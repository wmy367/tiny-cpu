/**********************************************
______________                ______________
______________ \  /\  /|\  /| ______________
______________  \/  \/ | \/ | ______________

descript:
mail: wmy367@gmail.com
Version: VERA.0.1 2018-3-22 16:48:16
    can set where pack_data at
Version: VERA.1.0 2018-6-22 15:42:27
    can be reset
creaded: 2017/8/3 下午6:02:50
madified:
***********************************************/
`timescale 1ns/1ps
module data_inf_c_planer_A1 #(
    parameter LAT   = 3,
    parameter DSIZE = 8,
    parameter HEAD  = "FALSE"
)(
    input                 reset,
    input [DSIZE-1:0]     pack_data,
    data_inf_c.slaver     slaver,
    data_inf_c.master     master        //{pack_data,slaver.data} or {slaver.data,pack_data} depen on HEAD
);


data_inf #(slaver.DSIZE) slaver_nc ();
data_inf #(master.DSIZE) master_nc ();

data_inf_A2B data_inf_A2B_planer_inst(
/*  data_inf.slaver   */  .slaver       (master_nc  ),
/*  data_inf_c.master */  .master       (master     )
);

data_inf_B2A data_inf_B2A_planer_inst(
/*  data_inf_c.slaver */  .slaver       (slaver     ),
/*  data_inf.master   */  .master       (slaver_nc  )
);

data_inf_planer_A1 #(
    .LAT    (LAT    ),
    .DSIZE  (DSIZE  ),
    .HEAD   (HEAD   )
)data_inf_planer_inst(
/*  input              */   .clock      (slaver.clock   ),
/*  input              */   .rst_n      (slaver.rst_n && !reset  ),
/*  input [DSIZE-1:0]  */   .pack_data  (pack_data  ),
/*  data_inf.slaver    */   .slaver     (slaver_nc  ),
/*  data_inf.master    */   .master     (master_nc  )        //{pack_data,slaver.data}
);

endmodule
