
/**********************************************
______________              ______________
______________ XPECTVISION  ______________
______________              ______________
descript:
author : Young
Version: VERA.0.0
created: 2018-12-15 10:34:38 +0800
madified:
***********************************************/
`timescale 1ns/1ps

module demo_tpu_ex_units #(
    parameter   WR_REG_NUM  = 8,
    parameter   RD_REG_NUM  = 7,
    parameter   TRI_REG_NUM = 5
)(
    input                           clock,
    input                           rst_n,
    output logic                    sda_out,
    output logic                    spi_mosi,
    input                           sda_in,
    input                           spi_miso,
    output logic[1-1:0]             scl,
    output logic[1-1:0]             tri_ctrl,

    output logic[1-1:0]             spi_sclk,
    output logic[1-1:0]             spi_csn,

    input  logic [TRI_REG_NUM-1:0]  trigger,
    input  logic [15:0]             wr_regs [WR_REG_NUM-1:0],
    input  logic [WR_REG_NUM-1:0]   wr_regs_update,
    output logic [15:0]             rd_regs [RD_REG_NUM-1:0],
    output logic [RD_REG_NUM-1:0]   rd_regs_update
);


//--->> ADD CAL <<-------------------------
always@(posedge clock,negedge rst_n)
    if(~rst_n)  {rd_regs[1],rd_regs[0]}    <= '0;
    else begin
        {rd_regs[1],rd_regs[0]}            <= wr_regs[0] + wr_regs[1];
    end
//---<< ADD CAL >>-------------------------

//--->> Multi CAL <<-------------------------
always@(posedge clock,negedge rst_n)
    if(~rst_n)begin
        rd_regs[2]    <= '0;
        rd_regs[3]    <= '0;
    end else begin
        {rd_regs[3],rd_regs[2]} <= wr_regs[0] * wr_regs[1];
    end
//---<< Multi CAL >>-------------------------

//--->> MIX CAL <<-------------------------
always@(posedge clock,negedge rst_n)
    if(~rst_n)begin
        rd_regs[4]    <= '0;
    end else begin
        for(int II=0;II<16;II++)begin
            if(wr_regs[2][II])
                    rd_regs[4][II] <= wr_regs[1][II];
            else    rd_regs[4][II] <= wr_regs[0][II];
        end
    end
//---<< MIX CAL >>-------------------------

//--->>  OUT  indep_out_iic <<-------------------------
logic[1-1:0]             indep_out_iic [2-1:0];
assign scl  = indep_out_iic[0];
assign tri_ctrl  = indep_out_iic[1];

always@(posedge clock,negedge rst_n)
    if(~rst_n)begin
        indep_out_iic[0]    <= 0;
        indep_out_iic[1]    <= 1;

    end else begin
        if(wr_regs_update[4])begin
            if(wr_regs[4][0+:(10-1)] < 2)begin
                indep_out_iic[wr_regs[4][0+:(10-1)]]  <= wr_regs[4][9-:1];
            end else begin
                ;
            end
        end else begin
            ;
        end
    end
//---<< OUT indep_out_iic >>-------------------------

//--->> Serial OUT [sda_out]<<-------------------------
logic [8-1:0]    sda_out_parallel;
always@(posedge clock,negedge rst_n)
    if(~rst_n)  sda_out_parallel    <= '0;
    else begin
        if(wr_regs_update[5])
                sda_out_parallel    <= wr_regs[5][8-1:0];
        else if(trigger[0])
            if(0)
                    sda_out_parallel    <= {sda_out_parallel[0],sda_out_parallel[8-1:1]};
            else    sda_out_parallel    <= {sda_out_parallel[8-2:0],sda_out_parallel[8-1]};
        else    sda_out_parallel    <= sda_out_parallel;
    end

always@(posedge clock,negedge rst_n)
    if(~rst_n)  sda_out     <= 1;
    else begin
        if(trigger[0])
            if(0)
                    sda_out     <= sda_out_parallel[0];
            else    sda_out     <= sda_out_parallel[8-1];
        else    sda_out     <= sda_out;
    end
//---<< Serial OUT [sda_out]>>-------------------------

//--->> Serial IN [sda_in]<<-------------------------
always@(posedge clock,negedge rst_n)
    if(~rst_n)  rd_regs[5]     <= '0;
    else begin
        if(trigger[1])
            if(1)
                    rd_regs[5]     <= {rd_regs[5][14:0],sda_in};
            else    rd_regs[5]     <= {sda_in,rd_regs[5][15:1]};
        else    rd_regs[5]     <= rd_regs[5];
    end
//---<< Serial IN [sda_in]>>-------------------------

//--->>  OUT  indep_out_spi <<-------------------------
logic[1-1:0]             indep_out_spi [2-1:0];
assign spi_sclk  = indep_out_spi[0];
assign spi_csn  = indep_out_spi[1];

always@(posedge clock,negedge rst_n)
    if(~rst_n)begin
        indep_out_spi[0]    <= 0;
        indep_out_spi[1]    <= 1;

    end else begin
        if(wr_regs_update[6])begin
            if(wr_regs[6][0+:(10-1)] < 2)begin
                indep_out_spi[wr_regs[6][0+:(10-1)]]  <= wr_regs[6][9-:1];
            end else begin
                ;
            end
        end else begin
            ;
        end
    end
//---<< OUT indep_out_spi >>-------------------------

//--->> Serial OUT [spi_mosi]<<-------------------------
logic [16-1:0]    spi_mosi_parallel;
always@(posedge clock,negedge rst_n)
    if(~rst_n)  spi_mosi_parallel    <= '0;
    else begin
        if(wr_regs_update[7])
                spi_mosi_parallel    <= wr_regs[7][16-1:0];
        else if(trigger[3])
            if(0)
                    spi_mosi_parallel    <= {spi_mosi_parallel[0],spi_mosi_parallel[16-1:1]};
            else    spi_mosi_parallel    <= {spi_mosi_parallel[16-2:0],spi_mosi_parallel[16-1]};
        else    spi_mosi_parallel    <= spi_mosi_parallel;
    end

always@(posedge clock,negedge rst_n)
    if(~rst_n)  spi_mosi     <= 1;
    else begin
        if(trigger[3])
            if(0)
                    spi_mosi     <= spi_mosi_parallel[0];
            else    spi_mosi     <= spi_mosi_parallel[16-1];
        else    spi_mosi     <= spi_mosi;
    end
//---<< Serial OUT [spi_mosi]>>-------------------------

//--->> Serial IN [spi_miso]<<-------------------------
always@(posedge clock,negedge rst_n)
    if(~rst_n)  rd_regs[6]     <= '0;
    else begin
        if(trigger[4])
            if(1)
                    rd_regs[6]     <= {rd_regs[6][14:0],spi_miso};
            else    rd_regs[6]     <= {spi_miso,rd_regs[6][15:1]};
        else    rd_regs[6]     <= rd_regs[6];
    end
//---<< Serial IN [spi_miso]>>-------------------------

endmodule
