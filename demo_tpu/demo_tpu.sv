/**********************************************
______________              ______________
______________ XPECTVISION  ______________
______________              ______________
descript:
author : Young
Version: VERA.0.0
created: 2018-12-15 10:34:38 +0800
madified:
***********************************************/
`timescale 1ns/1ps

module demo_tpu #(
    parameter INIT_FILE = "C:/work/TPU/tiny-cpu/demo_tpu/demo_tpu.coe",
    parameter RAM_SIZE  = 10,
    parameter DSIZE     = 16
)(
    output logic                    sda_out,
    output logic                    spi_mosi,
    input                           sda_in,
    input                           spi_miso,
    output logic[1-1:0]             scl,
    output logic[1-1:0]             tri_ctrl,

    output logic[1-1:0]             spi_sclk,
    output logic[1-1:0]             spi_csn,

    input                   update_live_enable,
    output logic            update_live_open,
    output logic            busy,
    axi_stream_inf.slaver   update_ram_inf          //DSIZE = 26
);

import demo_tpu_CFG_PKG::*;

logic[1-1:0]             indep_out_iic [2-1:0];logic[1-1:0]             indep_out_spi [2-1:0];
logic [TRI_REG_NUM-1:0]  trigger;
logic [15:0]             wr_regs [WR_REG_NUM-1:0];
logic [WR_REG_NUM-1:0]   wr_regs_update;
logic [15:0]             rd_regs [RD_REG_NUM-1:0];
logic [RD_REG_NUM-1:0]   rd_regs_update;
data_inf_c #(1)          trigger_inf [TRI_REG_NUM-1:0] (update_ram_inf.aclk,update_ram_inf.aresetn);

genvar KK;
generate
    for(KK=0;KK<TRI_REG_NUM;KK++)begin
        if((KK != 2))begin
            assign trigger[KK]  = trigger_inf[KK].valid && trigger_inf[KK].ready;
            assign trigger_inf[KK].ready    = 1'b1;
        end
    end
endgenerate

demo_tpu_ex_units #(
    .WR_REG_NUM     (WR_REG_NUM  ),
    .RD_REG_NUM     (RD_REG_NUM  ),
    .TRI_REG_NUM    (TRI_REG_NUM )
)cal_unit_inst(
/*  input                          */ .clock                (update_ram_inf.aclk    ),
/*  input                          */ .rst_n                (update_ram_inf.aresetn ),
/*  output                          */  .sda_out(sda_out),
/*  output                          */  .spi_mosi(spi_mosi),
/*  input                           */  .sda_in(sda_in),
/*  input                           */  .spi_miso(spi_miso),
/*   output logic[1-1:0]            */  .scl(scl),
/*   output logic[1-1:0]            */  .tri_ctrl(tri_ctrl),

/*   output logic[1-1:0]            */  .spi_sclk(spi_sclk),
/*   output logic[1-1:0]            */  .spi_csn(spi_csn),

/*  input  logic [TRI_REG_NUM-1:0] */ .trigger              (trigger        ),
/*  input  logic [15:0]            */ .wr_regs              (wr_regs        ),//[WR_REG_NUM-1:0],
/*  input  logic [WR_REG_NUM-1:0]  */ .wr_regs_update       (wr_regs_update ),
/*  output logic [15:0]            */ .rd_regs              (rd_regs        ),//[RD_REG_NUM-1:0],
/*  output logic [RD_REG_NUM-1:0]  */ .rd_regs_update       (rd_regs_update )
);

axi_stream_inf #(10)  Bread_inf (update_ram_inf.aclk,update_ram_inf.aresetn,update_ram_inf.aclken);
axi_stream_inf #(26)  Bread_ref_inf (update_ram_inf.aclk,update_ram_inf.aresetn,update_ram_inf.aclken);
tpu_ram_inf    #(.DSIZE(16),.RSIZE(10))   ram_inf ();

axi_stream_inf #(update_ram_inf.DSIZE)  update_ram_inf_main (update_ram_inf.aclk,update_ram_inf.aresetn,update_ram_inf.aclken);
axi_stream_inf #(update_ram_inf.DSIZE)  update_ram_inf_sub [1:0] (update_ram_inf.aclk,update_ram_inf.aresetn,update_ram_inf.aclken);

Tiny_CPU_core_A2 #(
    .DSIZE          (DSIZE          ),
    .RAM_SIZE       (RAM_SIZE       ),
//    .INIT_FILE      (INIT_FILE      ),
    .WR_REG_NUM     (WR_REG_NUM     ),
    .RD_REG_NUM     (RD_REG_NUM     ),
    .TRI_REG_NUM    (TRI_REG_NUM    )
)Tiny_CPU_core_inst(
/*  output logic [15:0]              */  .wr_regs               (wr_regs          ),//[WR_REG_NUM-1:0],
/*  output logic [WR_REG_NUM-1:0]    */  .wr_regs_update        (wr_regs_update   ),
/*  input  logic [15:0]              */  .rd_regs               (rd_regs          ),//[RD_REG_NUM-1:0],
/*  input  logic [RD_REG_NUM-1:0]    */  .rd_regs_update        (rd_regs_update   ),
/*  data_inf_c.master                */  .trigger_inf           (trigger_inf      ),//[TRI_REG_NUM-1:0],
/*  output logic                     */  .busy                  (busy             ),
/*  axi_stream_inf.slaver            */  .update_ram_inf        (update_ram_inf   ),  //DSIZE = 26
/*  axi_stream_inf.slaver            */  .Bread_inf             (Bread_inf        ),               // DSIZE = RAM_SIZE
/*  axi_stream_inf.master            */  .Bread_ref_inf         (Bread_ref_inf    ),  // DSIZE = RAM_SIZE + DSIZE
/*  tpu_ram_inf.master               */  .ram_inf               (ram_inf          ),
/*  input                            */  .update_live_enable    (update_live_enable),
/*  output logic                     */  .update_live_open      (update_live_open  )
);


data_inf_c #(16) delay_trigger_inf (update_ram_inf.aclk,update_ram_inf.aresetn);

assign delay_trigger_inf.data   = wr_regs[1];
assign delay_trigger_inf.valid  = trigger_inf[2].valid;
assign trigger_inf[2].ready = delay_trigger_inf.ready;

inf_time_delay_ctrl_verb #(
    .SYS_CLK_FREQ       (100.0),      // Mhz
    .STEP_Xms           (0.0),      //
    .STEP_Sim_Xms       (0.00032)
)inf_time_delay_ctrl_verb_inst(
/*  output logic      */  .done         (),
/*  data_inf_c.slaver */  .trigger_inf  (delay_trigger_inf  )       //
);


core_bram_default #(
    .INIT_FILE      (INIT_FILE)
)core_bram_default_inst(
/*  tpu_ram_inf.slaver   */    .ram_inf     (ram_inf        )
);


unuse_base_bread bread_inst(
/*  axi_stream_inf.slaver */  .Bread_inf            (Bread_inf      ),
/*  axi_stream_inf.master */  .Bread_ref_inf        (Bread_ref_inf  )
);

endmodule
