/**********************************************
______________              ______________
______________ XPECTVISION  ______________
______________              ______________
descript:
author : Young
Version: VERA.0.0
created: 2018-12-15 10:34:38 +0800
madified:
***********************************************/
`timescale 1ns/1ps

package demo_tpu_CFG_PKG;
parameter WR_REG_NUM    = 8;
parameter RD_REG_NUM    = 7;
parameter TRI_REG_NUM   = 5;
parameter TPU_DSIZE     = 16;
parameter TPU_RAM_SIZE  = 10;
endpackage
