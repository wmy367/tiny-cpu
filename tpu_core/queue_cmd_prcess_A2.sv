/**********************************************
______________                ______________
______________ \  /\  /|\  /| ______________
______________  \/  \/ | \/ | ______________

descript:
mail: wmy367@gmail.com
Version: VERA.1.0  2018-6-21 14:16:33
    compact cmd
Version: VERA.2.0  2018-6-21 15:41:45
    data = 18
    addr = [10-12]
Version: VERA.2.1  2018/7/9 下午4:00:41
    add cmd CMD_DIRE_RD_RAM_W1
creaded: 2018-3-26 11:52:31
madified:
***********************************************/
`timescale 1ns/1ps
module queue_cmd_process_A2 #(
    parameter   WR_REG_NUM = 8,
    parameter   RD_REG_NUM = 8,
    parameter   TRI_REG_NUM = 8,
    parameter   RAM_SIZE    = 10,
    parameter   DSIZE       = 18
)(
    // input                           start,
    data_inf_c.slaver               trigger_start,
    output logic [DSIZE-1:0]        wr_regs [WR_REG_NUM-1:0],
    output logic [WR_REG_NUM-1:0]   wr_regs_update,
    input  logic [DSIZE-1:0]        rd_regs [RD_REG_NUM-1:0],
    input  logic [RD_REG_NUM-1:0]   rd_regs_update,
    // trigger inf
    data_inf_c.master               trigger_inf [TRI_REG_NUM-1:0],
    // jump cmd
    output logic                    jump,
    output logic [RAM_SIZE-1:0]     jump_addr,
    // wr back to ram
    output logic                    update,
    output logic [RAM_SIZE-1:0]     update_addr,
    output logic [DSIZE-1:0]        update_data,
    // rd from ram
    output logic                    rd_ram_en,
    output logic [RAM_SIZE-1:0]     rd_ram_addr,
    input [DSIZE-1:0]               rd_ram_data,        //delay 2 clock
    // queue
    data_inf_c.master               proc_done_inf,
    output logic                    queue_reset,
    data_inf_c.slaver               queue_inf,
    // update live ctrl
    input  logic                    update_live_enable,
    output logic                    update_live_open
);

localparam  WR_REG_DSIZE = $clog2(WR_REG_NUM);
localparam  RD_REG_DSIZE = $clog2(RD_REG_NUM);
localparam  TRI_REG_DSIZE = (TRI_REG_NUM<=1)? 1 : $clog2(TRI_REG_NUM);

import TPU_CMD_PKG::*;

logic       clock,rst_n;

assign  clock   = queue_inf.clock;
assign  rst_n   = queue_inf.rst_n;

logic [DSIZE-1:0]           cache_w0,cache_w1;
logic [DSIZE-1:0]           cache_r0;
logic [TRI_REG_NUM-1:0]     trigger_vld;
logic [TRI_REG_NUM-1:0]     trigger_rdy;

genvar KK;
generate
for(KK=0;KK<TRI_REG_NUM;KK++)begin
    assign trigger_inf[KK].valid = trigger_vld[KK];
    assign trigger_rdy[KK]       = trigger_inf[KK].ready;
end
endgenerate
(* dont_touch = "true" *)
logic [RAM_SIZE-1:0]    curr_addr;
logic [5:0]             curr_cmd;
logic [DSIZE-6-1:0]     curr_data;
logic                   req_bak;
logic                   req_bak_done;
logic                   rd_reg_vld;
logic                   delay_done;
logic                   w0_smaller_w1;
logic                   w0_eql_w1;
logic                   trigger_done;
logic                   wait_hold_done;

always@(posedge clock,negedge rst_n)
    if(~rst_n)  w0_smaller_w1   <= 1'b0;
    else        w0_smaller_w1   <= cache_w0 < cache_w1;

always@(posedge clock,negedge rst_n)
    if(~rst_n)  w0_eql_w1   <= 1'b0;
    else        w0_eql_w1   <= cache_w0 == cache_w1;

assign  {curr_cmd,curr_data}    = queue_inf.data[DSIZE-1:0];
assign  curr_addr               = queue_inf.data[DSIZE+:RAM_SIZE];

typedef enum {
        NOP,
        IDLE,
        JUMP,
        LOAD_W0,
        LOAD_W1,
        SET_W0_TO_WG_1,
        SET_W1_TO_WG_1,
        RD_TO_R0,
        R0_TO_RAM,
        W0_TO_R0,
        W1_TO_R0,
        WG_TO_R0,
        R0_TO_W0,
        // W0BASE_TO_RAM,
        DELAY,
        DELAY_WAIT,
        W0_TO_TG,
        W0_TO_TG_WAIT,
        W0_W1_JUMP_S,
        W0_W1_JUMP_EQL,
        CLEAR_NEXT_CMD,
        SET_JUMP,
        //---- 2018-5-11 18:26:00
        LOAD_WG_16bit,
        LOAD_WG_16bit_DATA,
        //---- 2018-5-23 16:57:17
        DIRE_RD_RAM_SET,
        DIRE_RD_RAM_GET,
        //---- 2018-6-4 17:15:48
        R0_TO_W1,
        //---- 2018-6-28 11:47:38
        W0_W1_TO_RAM,
        //---- 2018/7/9 下午4:01:03
        DIRE_RD_RAM_W1,
        //---- 2018/8/10 上午9:40:03
        UPDATE_LIVE,
        UPDATING,
        PROC_END }        STATUS;

STATUS  nstate,cstate;

always@(posedge clock,negedge rst_n)
    if(~rst_n)  cstate  <= NOP;
    else        cstate  <= nstate;


function STATUS router_status(input vld_rdy,input [5:0]  cmd,input STATUS curr_s);
    if(vld_rdy)begin
        case(cmd)
        CMD_JUMP:       router_status = JUMP;
        CMD_LOAD_W0:    router_status = LOAD_W0;
        CMD_LOAD_W1:    router_status = LOAD_W1;
        CMD_SET_W0_TO_WG_1: router_status = SET_W0_TO_WG_1;
        CMD_SET_W1_TO_WG_1: router_status = SET_W1_TO_WG_1;
        // CMD_SET_W0_TO_WG_2:
        //         router_status = SET_W0_TO_WG_2;
        // CMD_SET_W1_TO_WG_2:
        //         router_status = SET_W1_TO_WG_2;
        CMD_RD_TO_R0:           router_status = RD_TO_R0;
        CMD_R0_TO_RAM:          router_status = R0_TO_RAM;
        // CMD_W0BASE_TO_RAM:      router_status = W0BASE_TO_RAM;
        CMD_IDLE_DLY:           router_status = DELAY;
        CMD_W0_TO_TG:           router_status = W0_TO_TG;
        CMD_W0_W1_JUMP_S:       router_status = W0_W1_JUMP_S;
        CMD_W0_W1_JUMP_EQL:     router_status = W0_W1_JUMP_EQL;
        CMD_R0_TO_W0:           router_status = R0_TO_W0;
        CMD_PROC_END:           router_status = PROC_END;
        CMD_WG_TO_R0:           router_status = WG_TO_R0;
        CMD_W0_TO_R0:           router_status = W0_TO_R0;
        CMD_W1_TO_R0:           router_status = W1_TO_R0;
        //--- 2018-5-11 18:26:17
        CMD_LOAD_WG_16bit:      router_status = LOAD_WG_16bit;
        //--- 2018-5-23 16:55:40
        CMD_DIRE_RD_RAM_SET:    router_status = DIRE_RD_RAM_SET;
        CMD_DIRE_RD_RAM_GET:    router_status = DIRE_RD_RAM_GET;
        //--- 2018/7/9 下午4:01:59
        CMD_DIRE_RD_RAM_W1:     router_status = DIRE_RD_RAM_W1;
        CMD_R0_TO_W1:           router_status = R0_TO_W1;
        //--- 2018-6-28 11:51:09
        CMD_W0_W1_TO_RAM:       router_status = W0_W1_TO_RAM;
        //--- 2018/8/10 上午9:41:53
        CMD_UPDATE_LIVE:        router_status = UPDATE_LIVE;
        default:                router_status = IDLE;
        endcase
    end else    router_status = curr_s;
endfunction:router_status

always_comb begin
    case(cstate)
    NOP:begin
        if(trigger_start.valid && trigger_start.ready)
                nstate = IDLE;
        else    nstate = NOP;
    end
    IDLE:begin
        nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,IDLE);
    end
    JUMP:               nstate = IDLE;
    // W0BASE_TO_RAM:      nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,W0BASE_TO_RAM);
    LOAD_W0:            nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,LOAD_W0);
    LOAD_W1:            nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,LOAD_W1);
    WG_TO_R0:           nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,WG_TO_R0);
    W0_TO_R0:           nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,W0_TO_R0);
    W1_TO_R0:           nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,W1_TO_R0);
    SET_W0_TO_WG_1:begin
        nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,SET_W0_TO_WG_1);
    end
    SET_W1_TO_WG_1:begin
        nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,SET_W1_TO_WG_1);
    end
    RD_TO_R0:           nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,RD_TO_R0);
    R0_TO_RAM:begin
        nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,R0_TO_RAM);
    end
    DELAY:begin
        nstate  = DELAY_WAIT;
    end
    DELAY_WAIT:begin
        if(delay_done)
                nstate = IDLE;
        else    nstate = DELAY_WAIT;
    end
    W0_TO_TG:
        // if(queue_inf.valid & queue_inf.ready)
                nstate = W0_TO_TG_WAIT;
        // else    nstate = W0_TO_TG;
    W0_TO_TG_WAIT:begin
        if(trigger_done)
                nstate  = IDLE;
        else    nstate  = W0_TO_TG_WAIT;
    end
    W0_W1_JUMP_S:begin
        if(w0_smaller_w1)
                nstate = CLEAR_NEXT_CMD;
        else    nstate = IDLE;
    end
    W0_W1_JUMP_EQL:begin
        if(w0_eql_w1)
                nstate = CLEAR_NEXT_CMD;
        else    nstate = IDLE;
    end
    CLEAR_NEXT_CMD:
                nstate = SET_JUMP;
    PROC_END:
        if(proc_done_inf.valid && proc_done_inf.ready)
                nstate = NOP;
        else    nstate = PROC_END;
    SET_JUMP:   nstate = IDLE;
    //--- 2018-5-11 18:26:41
    LOAD_WG_16bit:
        if(queue_inf.valid & queue_inf.ready)
                nstate = LOAD_WG_16bit_DATA;
        else    nstate = LOAD_WG_16bit;
    LOAD_WG_16bit_DATA:
                nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,IDLE);
    DIRE_RD_RAM_SET:
                nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,DIRE_RD_RAM_SET);
    DIRE_RD_RAM_GET:
                nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,DIRE_RD_RAM_GET);
    //--- 2018-6-4 17:16:14
    R0_TO_W1:   nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,R0_TO_W1);
    //--- 2018-6-19 10:52:51
    R0_TO_W0:   nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,R0_TO_W0);
    //--- 2018-6-28 11:53:45
    W0_W1_TO_RAM:
                nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,W0_W1_TO_RAM);
    //--- 2018/7/9 下午4:02:28
    DIRE_RD_RAM_W1:
                nstate = router_status(queue_inf.valid & queue_inf.ready,curr_cmd,DIRE_RD_RAM_W1);
    //--- 2018/8/10 上午9:42:55
    UPDATE_LIVE:
                // nstate = router_status(update_live_enable,curr_cmd,UPDATING);
        if(update_live_enable)
                nstate  = UPDATING;
        else    nstate = router_status(!update_live_enable,curr_cmd,UPDATE_LIVE);
    UPDATING:
        if(!update_live_enable)
                nstate  = SET_JUMP;
        else    nstate  = UPDATING;
    default:    nstate = IDLE;
    endcase
end

//-------------------------------------------------------------------------------
//---->> UPDATE CTRL <<---------------------------
always@(posedge clock,negedge rst_n)begin
    if(~rst_n)  update  <= 1'b0;
    else begin
        case(nstate)
        W0_W1_TO_RAM,
        // W0BASE_TO_RAM,
        R0_TO_RAM:          update  <= queue_inf.valid && queue_inf.ready;
        default:            update  <= 1'b0;
        endcase
    end
end

always@(posedge clock,negedge rst_n)begin
    if(~rst_n)  update_data  <= 'b0;
    else begin
        case(nstate)
        // W0BASE_TO_RAM:      update_data  <= cache_w0;
        R0_TO_RAM:          update_data  <= cache_r0;
        W0_W1_TO_RAM:       update_data  <= cache_w0;
        default:;
        endcase
    end
end

always@(posedge clock,negedge rst_n)begin
    if(~rst_n)  update_addr  <= 'b0;
    else begin
        case(nstate)
        // W0BASE_TO_RAM:      update_addr     <= queue_inf.data[RAM_SIZE-1:0];
        R0_TO_RAM:          update_addr     <= queue_inf.data[RAM_SIZE-1:0];
        W0_W1_TO_RAM:       update_addr     <= cache_w1;
        default:;
        endcase
    end
end
//----<< UPDATE CTRL >>---------------------------
//---->> READ CACHE CTRL <<---------------------------
always@(posedge clock,negedge rst_n)begin
    if(~rst_n)  cache_r0    <= 'b0;
    else begin
        case(nstate)
        RD_TO_R0:       cache_r0        <= rd_regs[queue_inf.data[0+:RD_REG_DSIZE]];
        W0_TO_R0:       cache_r0        <= cache_w0;
        W1_TO_R0:       cache_r0        <= cache_w1;
        WG_TO_R0:       cache_r0        <= wr_regs[queue_inf.data[0+:WR_REG_DSIZE]];
        default:;
        endcase
    end
end
//----<< READ CACHE CTRL >>---------------------------
//---->> WRITE REG CTRL <<---------------------------
always@(posedge clock,negedge rst_n)begin
    if(~rst_n);
    else begin
        case(nstate)
        SET_W0_TO_WG_1: wr_regs[queue_inf.data[0+:WR_REG_DSIZE]]   <= cache_w0;
        SET_W1_TO_WG_1: wr_regs[queue_inf.data[0+:WR_REG_DSIZE]]   <= cache_w1;
        LOAD_WG_16bit_DATA:
                        wr_regs[cache_w0[0+:WR_REG_DSIZE]]         <= queue_inf.data;
        // SET_W0_TO_WG_1: wr_regs[queue_inf.data[0+:WR_REG_DSIZE]]   <= 0;
        default:;
        endcase
    end
end

always@(posedge clock,negedge rst_n)begin
    if(~rst_n)  wr_regs_update = 'b0;
    else begin
        wr_regs_update = 'b0;
        case(nstate)
        SET_W0_TO_WG_1: wr_regs_update[queue_inf.data[0+:WR_REG_DSIZE]]   <= 1'b1;
        SET_W1_TO_WG_1: wr_regs_update[queue_inf.data[0+:WR_REG_DSIZE]]   <= 1'b1;
        LOAD_WG_16bit_DATA:
                        wr_regs_update[cache_w0[0+:WR_REG_DSIZE]]         <= 1'b1;
        default:;
        endcase
    end
end
//----<< WRITE REG CTRL >>---------------------------
//---->> CACHE W0 Ctrl <<----------------------------
always@(posedge clock,negedge rst_n)begin
    if(~rst_n)  cache_w0    <= 'b0;
    else begin
        case(nstate)
        LOAD_WG_16bit:
                        cache_w0    <= queue_inf.data[DSIZE-1:0];
        W0_TO_TG,W0_W1_JUMP_S,W0_W1_JUMP_EQL,
        RD_TO_R0:
                        cache_w0    <= queue_inf.data[DSIZE-1-6:0];
        LOAD_W0:        cache_w0    <= queue_inf.data[DSIZE-1-6:0];
        R0_TO_W0:       cache_w0    <= cache_r0;
        UPDATING:       cache_w0    <= 'b0;
        default:;
        endcase
    end
end

always@(posedge clock,negedge rst_n)
    if(~rst_n)  rd_reg_vld  <= 1'b0;
    else
        case(nstate)
        RD_TO_R0:
                rd_reg_vld  <= rd_regs_update[queue_inf.data[RD_REG_DSIZE-1:0]];
        default:rd_reg_vld  <= rd_regs_update[cache_w0[RD_REG_DSIZE-1:0]];
        endcase
//----<< CACHE W0 Ctrl >>----------------------------
//---->> CACHE W1 Ctrl <<----------------------------
always@(posedge clock,negedge rst_n)begin
    if(~rst_n)  cache_w1    <= 'b0;
    else begin
        case(nstate)
        DELAY,
        LOAD_W1:    cache_w1    <= queue_inf.data[DSIZE-1-6:0];
        DELAY_WAIT:begin
            if(cache_w1 != 0)
                    cache_w1    <= cache_w1 - 1'b1;
            else    cache_w1    <= cache_w1;
        end
        DIRE_RD_RAM_GET:
                    cache_w1    <= rd_ram_data;
        R0_TO_W1:   cache_w1    <= cache_r0;
        default:;
        endcase
    end
end

always@(posedge clock,negedge rst_n)
    if(~rst_n)   delay_done <= 1'b0;
    else
        case(nstate)
        DELAY_WAIT: delay_done <= cache_w1 == 'b0;
        default:    delay_done <= 1'b0;
        endcase
//----<< CACHE W1 Ctrl >>----------------------------
//--->> QUEUE Ctrl <<--------------------------------
always@(posedge clock,negedge rst_n)begin
    if(~rst_n)  queue_inf.ready     <= 1'b0;
    else begin
        case(nstate)
        NOP:    queue_inf.ready     <= 1'b0;
        IDLE:   queue_inf.ready     <= 1'b1;
        DELAY,
        DELAY_WAIT,
        W0_TO_TG,
        W0_TO_TG_WAIT,
        PROC_END:
                queue_inf.ready     <= 1'b0;
        default:queue_inf.ready     <= 1'b1;
        endcase
    end
end
always@(posedge clock,negedge rst_n)
    if(~rst_n)  queue_reset <= 1'b0;
    else
        case(nstate)
        PROC_END,
        JUMP,
        CLEAR_NEXT_CMD,
        SET_JUMP:   queue_reset <= 1'b1;
        UPDATING:   queue_reset <= 1'b1;
        default:    queue_reset <= 1'b0;
        endcase
//---<< QUEUE Ctrl >>--------------------------------
//--->> TRIGGER Ctrl <<------------------------------
always@(posedge clock,negedge rst_n)
    if(~rst_n)  trigger_vld <= 'b0;
    else begin
        case(nstate)
        W0_TO_TG:begin
            trigger_vld[queue_inf.data[TRI_REG_DSIZE-1:0]]    <= 1'b1;
        end
        W0_TO_TG_WAIT:begin
            foreach(trigger_rdy[i])begin
                if(trigger_rdy[i] && i==cache_w0[TRI_REG_DSIZE-1:0])
                        trigger_vld[i]  <= 1'b0;
                else    trigger_vld[i]  <= trigger_vld[i];
            end
        end
        default:  trigger_vld <= 'b0;
        endcase
    end

always@(posedge clock,negedge rst_n)
    if(~rst_n)  trigger_done <= 'b0;
    else begin
        trigger_done    <= |(trigger_vld & trigger_rdy);
    end

//---<< TRIGGER Ctrl >>------------------------------
//--->> JUMP Ctrl <<---------------------------------
always@(posedge clock,negedge rst_n)
    if(~rst_n)  jump    <= 1'b0;
    else
        case(nstate)
        SET_JUMP,
        JUMP:   jump    <= 1'b1;
        default:jump    <= 1'b0;
        endcase

always@(posedge clock,negedge rst_n)
    if(~rst_n)  jump_addr   <= 'b0;
    else
        case(nstate)
        SET_JUMP:   jump_addr   <= cache_w0[RAM_SIZE-1:0];
        JUMP:       jump_addr   <= queue_inf.data[RAM_SIZE-1:0];
        default:;
        endcase
//---<< JUMP Ctrl >>---------------------------------
//--->> PROC DONE FLAG <<----------------------------
always@(posedge clock,negedge rst_n)
    if(~rst_n)  proc_done_inf.valid   <= 1'b0;
    else
        case(nstate)
        PROC_END:   proc_done_inf.valid   <= 1'b1;
        default:    proc_done_inf.valid   <= 1'b0;
        endcase
//---<< PROC DONE FLAG >>----------------------------
//--->> TRIGGER START <<-----------------------------
always@(posedge clock,negedge rst_n)
    if(~rst_n)  trigger_start.ready   <= 1'b0;
    else
        case(nstate)
        NOP:        trigger_start.ready   <= 1'b1;
        default:    trigger_start.ready   <= 1'b0;
        endcase
//---<< TRIGGER START >>-----------------------------
//--->> RD FROM RAM <<-------------------------------
always@(posedge clock,negedge rst_n)
    if(~rst_n)  rd_ram_en   <= 1'b0;
    else
        case(nstate)
        DIRE_RD_RAM_SET:
                rd_ram_en   <= 1'b1;
        DIRE_RD_RAM_W1:
                rd_ram_en   <= 1'b1;
        default:rd_ram_en   <= 1'b0;
        endcase

always@(posedge clock,negedge rst_n)
    if(~rst_n)  rd_ram_addr   <= '0;
    else
        case(nstate)
        DIRE_RD_RAM_SET:
                rd_ram_addr   <= queue_inf.data[RAM_SIZE-1:0];
        DIRE_RD_RAM_W1:
                rd_ram_addr   <= cache_w1[RAM_SIZE-1:0];
        default:rd_ram_addr   <= '0;
        endcase
//---<< RD FROM RAM >>-------------------------------
//--->> OPEN UPDATE <<-------------------------------
always@(posedge clock,negedge rst_n)
    if(~rst_n)  update_live_open   <= '0;
    else
        case(nstate)
        UPDATING:
                update_live_open   <= 1'b1;
        default:update_live_open   <= '0;
        endcase
//---<< OPEN UPDATE >>-------------------------------
endmodule
