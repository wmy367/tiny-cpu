/**********************************************
______________                ______________
______________ \  /\  /|\  /| ______________
______________  \/  \/ | \/ | ______________

descript:
mail: wmy367@gmail.com
Version: VERA.2.0 2018-6-21 15:29:10
    width == 18
creaded: 2018/7/2 上午10:04:29
madified:
***********************************************/
`timescale 1ns/1ps
module core_bram_default #(
    parameter INIT_FILE = "NONE"
)(
    tpu_ram_inf.slaver          ram_inf
);

logic [31:0]    ADDRA,ADDRB;

// assign  ADDRA = ram_inf.addra<<5;
// assign  ADDRB = ram_inf.addrb<<5;
//
// core_bram_A2 #(
//     .INIT_FILE      (INIT_FILE      ),
//     .RAM_SIZE       (ram_inf.RSIZE  ),
//     .DSIZE          (ram_inf.DSIZE  )
// )core_bram_A2_inst(
// /*  input                   */  .CLKA       (ram_inf.clka ),
// /*  input                   */  .RSTA       (ram_inf.rsta ),
// /*  input [15:0]            */  .ADDRA      (ADDRA        ),
// /*  input [DSIZE-1:0]       */  .DIA        (ram_inf.dia  ),
// /*  input                   */  .ENA        (ram_inf.ena  ),
// /*  input                   */  .WEA        (ram_inf.wea  ),
// /*  output logic[DSIZE-1:0] */  .DOA        (ram_inf.doa  ),
//
// /*  input                   */  .CLKB       (ram_inf.clkb ),
// /*  input                   */  .RSTB       (ram_inf.rstb ),
// /*  input [15:0]            */  .ADDRB      (ADDRB        ),
// /*  input [DSIZE-1:0]       */  .DIB        (ram_inf.dib  ),
// /*  input                   */  .ENB        (ram_inf.enb  ),
// /*  input                   */  .WEB        (ram_inf.web  ),
// /*  output logic[DSIZE-1:0] */  .DOB        (ram_inf.dob  )
//
// );

assign  ADDRA = ram_inf.addra;
assign  ADDRB = ram_inf.addrb;

core_bram_sim #(
    .INIT_FILE      (INIT_FILE      ),
    .RAM_SIZE       (ram_inf.RSIZE  ),
    .DSIZE          (ram_inf.DSIZE  )
)core_bram_sim_inst(
/*  input                   */  .CLKA       (ram_inf.clka ),
/*  input                   */  .RSTA       (ram_inf.rsta ),
/*  input [15:0]            */  .ADDRA      (ADDRA[15:0]  ),
/*  input [DSIZE-1:0]       */  .DIA        (ram_inf.dia  ),
/*  input                   */  .ENA        (ram_inf.ena  ),
/*  input                   */  .WEA        (ram_inf.wea  ),
/*  output logic[DSIZE-1:0] */  .DOA        (ram_inf.doa  ),

/*  input                   */  .CLKB       (ram_inf.clkb ),
/*  input                   */  .RSTB       (ram_inf.rstb ),
/*  input [15:0]            */  .ADDRB      (ADDRB[15:0]  ),
/*  input [DSIZE-1:0]       */  .DIB        (ram_inf.dib  ),
/*  input                   */  .ENB        (ram_inf.enb  ),
/*  input                   */  .WEB        (ram_inf.web  ),
/*  output logic[DSIZE-1:0] */  .DOB        (ram_inf.dob  )

);

endmodule
