package TPU_CMD_PKG;

localparam
            CMD_LOAD_W0             = 5'h01,
            CMD_LOAD_W1             = 5'h02,
            CMD_SET_W0_TO_WG_1      = 5'h03,
            CMD_SET_W1_TO_WG_1      = 5'h04,
            CMD_RD_TO_R0            = 5'h07,
            CMD_R0_TO_RAM           = 5'h08,
            CMD_W0_TO_R0            = 5'h09,
            CMD_W1_TO_R0            = 5'h0A,
            CMD_WG_TO_R0            = 5'h0B,
            // CMD_W0BASE_TO_RAM       = 5'h0C,
            // CMD_RD_TO_R0_with_V     = 5'h0D,
            CMD_W0_W1_TO_RAM        = 5'h0D,
            CMD_IDLE_DLY            = 5'h0E,
            // CMD_MUTEX_LOCK          = 5'h0F,
            // CMD_MUTEX_UNLOCK        = 5'h10,
            CMD_W0_TO_TG            = 5'h11,
            CMD_WAIT_HOLD           = 5'h12,
            CMD_W0_W1_JUMP_S        = 5'h13,
            CMD_W0_W1_JUMP_EQL      = 5'h14,
            CMD_R0_TO_W0            = 5'h15,
            CMD_R0_TO_W1            = 5'h19,
            CMD_PROC_END            = 5'h1A,
            CMD_IDLE                = 5'h1B,
            CMD_JUMP                = 5'h1C,
            CMD_LOAD_WG_16bit       = 5'h1D,
            CMD_DIRE_RD_RAM_SET     = 5'h1E,
            CMD_DIRE_RD_RAM_GET     = 5'h1F,
            CMD_DIRE_RD_RAM_W1      = 5'h0F, // -- 2018/7/9 下午4:00:03
            CMD_UPDATE_LIVE         = 5'h0c, // -- 2018/8/10 上午9:40:48
            CMD_UPDATE_LIVR_RESTART = 5'h1B; // -- 2018/8/14 下午4:31:29

endpackage
