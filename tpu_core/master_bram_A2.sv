/**********************************************
______________                ______________
______________ \  /\  /|\  /| ______________
______________  \/  \/ | \/ | ______________

descript:
mail: wmy367@gmail.com
Version: VERB.0.0 2018-5-10 17:16:01
    include gen_addr_data_verb
Version: VERA.2.0 2018-6-21 15:51:58
    width == 18
    addr == 12
creaded: 2018-3-22 14:08:07
madified:
***********************************************/
`timescale 1ns/1ps
module master_bram_A2 #(
    // parameter INIT_FILE     = "NONE",
    parameter OUT_LAT       = 3,
    parameter RAM_SIZE      = 10,
    parameter DSIZE         = 18
)(
    //-- A SIZE
    input                   clock,
    input                   rst_n,
    data_inf_c.slaver       boot_start_inf,
    //---- ADDR DATA Ctrl
    input                   jump,
    input [RAM_SIZE-1:0]    jump_addr,
    input                   update,
    input [RAM_SIZE-1:0]    update_addr,
    input [DSIZE-1:0]       update_data,
    input                   rd_ram_en,
    input [RAM_SIZE-1:0]    rd_ram_addr,
    output[DSIZE-1:0]       rd_ram_data,
    data_inf_c.mirror       proc_done_inf,
    //---- QUEUE Ctrl
    input                   queue_reset,
    data_inf_c.master       queue_inf,
    //-- B SIZE
    axi_stream_inf.slaver   Bread_inf,      // DSIZE = RAM_SIZE
    axi_stream_inf.master   Bread_ref_inf,  // DSIZE = RAM_SIZE+18
    axi_stream_inf.slaver   Bwrite_inf,      // DSIZE = RAM_SIZE+18
    // ram interfaec
    tpu_ram_inf.master      ram_inf
);


initial begin

    assert(queue_inf.DSIZE == (RAM_SIZE+DSIZE))
    else begin
        $error("QUEUE DSIZE[%d] MUST EQL (RAM_SIZE+DSIZE)",queue_inf.DSIZE);
        $stop;
    end

    assert(Bread_inf.DSIZE == RAM_SIZE)
    else begin
        $error("BREAD DSIZE[%d] MUST EQL RAM_SIZE",Bread_inf.DSIZE);
        $stop;
    end

    assert(Bread_ref_inf.DSIZE == (RAM_SIZE+DSIZE))
    else begin
        $error("BREAD REL DSIZE[%d] MUST EQL (RAM_SIZE+DSIZE)",Bread_ref_inf.DSIZE);
        $stop;
    end

    assert(Bwrite_inf.DSIZE == (RAM_SIZE+DSIZE))
    else begin
        $error("BWRITE DSIZE[%d] MUST EQL (RAM_SIZE+DSIZE)",Bwrite_inf.DSIZE);
        $stop;
    end
end


//--->> ADDR CTRL <<------------------------------------
// bit [RAM_SIZE-1:0]    addra;

// logic                 addra_valid;
logic [DSIZE-1:0]     dataa;
logic                 wena;

logic [RAM_SIZE-1:0]        addra_lat   [OUT_LAT-1:0]  ;
logic [OUT_LAT-1:0]         addra_valid_lat;

data_inf_c #(RAM_SIZE)      addra_inf        (clock,rst_n);
data_inf_c #(RAM_SIZE)      addra_inf_and_we (clock,rst_n);

gen_addr_data_A2 #(
    .CORE_ID        (0  ),
    .ID_BASE_ADDR   (1024-32),
    .RAM_SIZE       (RAM_SIZE)
)gen_addr_data_A2_inst(
/*  input               */    .clock            (clock      ),
/*  input               */    .rst_n            (rst_n      ),
/*  input               */    .wr_core_id       (1'b0       ),
/*  input               */    .queue_reset      (queue_reset    ),
/*  data_inf_c.slaver   */    .boot_start_inf   (boot_start_inf ),
/*  input               */    .proc_done        (proc_done_inf.valid && proc_done_inf.ready),
/*  input               */    .jump             (jump           ),
/*  input [9:0]         */    .jump_addr        (jump_addr      ),
/*  input               */    .update           (update         ),
/*  input [9:0]         */    .update_addr      (update_addr    ),
/*  input [15:0]        */    .update_data      (update_data    ),
// /*  output logic        */    .valida           (addra_valid    ),
// /*  output logic[9:0]   */    .addra            (addra          ),
// /*  input [9:0]         */    .bak_addra        (addra_lat[OUT_LAT-1]-1),
/*  data_inf_c.master   */    .addra_inf        (addra_inf      ),
/*  output logic[15:0]  */    .dataa            (dataa          ),
/*  output logic        */    .wena             (wena           ),
/*  data_inf_c.mirror   */    .queue_inf        (queue_inf      )
);

assign  addra_inf_and_we.data   = addra_inf.data;
assign  addra_inf_and_we.valid  = addra_inf.valid && !wena;
assign  addra_inf.ready         = addra_inf_and_we.ready;

//---<< ADDR CTRL >>------------------------------------
logic [DSIZE-1:0]    DOA;

logic               CLKB;
logic               RSTB;
bit   [13+2:0]      ADDRB;
bit   [13+2:0]      ADDRA;
logic [DSIZE-1:0]   DIB;
logic               ENB;
logic               WEB;
logic [DSIZE-1:0]   DOB;

assign ADDRA = addra_inf_and_we.data;

// core_bram_A2 #(
//     .INIT_FILE   (INIT_FILE ),
//     .RAM_SIZE    (RAM_SIZE  )
// )core_bram_inst(
// /*  input           */    .CLKA     (clock  ),
// /*  input           */    .RSTA     (~rst_n ),
// /*  input [13:0]     */   .ADDRA    (ADDRA<<0  ),
// /*  input [15:0]    */    .DIA      (dataa  ),
// /*  input           */    .ENA      (1'b1   ),
// /*  input           */    .WEA      (wena   ),
// /*  output[15:0]    */    .DOA      (DOA    ),
// /*  input           */    .CLKB     (CLKB   ),
// /*  input           */    .RSTB     (RSTB   ),
// /*  input [13:0]     */   .ADDRB    (ADDRB<<0      ),
// /*  input [15:0]    */    .DIB      (DIB        ),
// /*  input           */    .ENB      (ENB        ),
// /*  input           */    .WEB      (WEB        ),
// /*  output[15:0]    */    .DOB      (DOB        )
// );

assign ram_inf.clka     = clock;
assign ram_inf.rsta     = ~rst_n;
assign ram_inf.addra    = ADDRA;
assign ram_inf.dia      = dataa;
assign ram_inf.ena      = 1'b1;
assign ram_inf.wea      = {ram_inf.MSIZE{wena}};
assign DOA              = ram_inf.doa;

assign ram_inf.clkb     = clock;
assign ram_inf.rstb     = ~rst_n;
assign ram_inf.addrb    = ADDRB;
assign ram_inf.dib      = DIB;
assign ram_inf.enb      = ENB;
assign ram_inf.web      = {ram_inf.MSIZE{WEB}};
assign DOB              = ram_inf.dob;
assign rd_ram_data      = DOB;

data_inf_c_planer_A1 #(
    .LAT    (OUT_LAT    ),
    .DSIZE  (DSIZE      ),
    .HEAD   ("FALSE"    )
)data_inf_c_planer_A1_inst(
/*  input             */  .reset        (queue_reset        ),
/*  input [DSIZE-1:0] */  .pack_data    (DOA                ),
/*  data_inf_c.slaver */  .slaver       (addra_inf_and_we   ),
/*  data_inf_c.master */  .master       (queue_inf          )       //{pack_data,slaver.data} or {slaver.data,pack_data} depen on HEAD
);

//--->> QUEUE <<-------------------------------------
// assign queue_inf.valid  = addra_valid_lat[OUT_LAT-1] && !queue_reset;
// assign queue_inf.data   = {addra_lat[OUT_LAT-1],DOA};
//---<< QUEUE >>-------------------------------------
//--->> B SIZE <<------------------------------------
assign CLKB     = Bread_inf.aclk;
assign RSTB     = ~Bread_inf.aresetn;

axi_stream_inf #(RAM_SIZE+DSIZE+1)  s00[1:0] (Bread_inf.aclk,Bread_inf.aresetn,1'b1);
axi_stream_inf #(RAM_SIZE+DSIZE+1)  m00      (Bread_inf.aclk,Bread_inf.aresetn,1'b1);
data_inf_c #(RAM_SIZE+1)            m00_post      (Bread_inf.aclk,Bread_inf.aresetn);
data_inf_c #(RAM_SIZE+DSIZE+1)      planer_m00    (Bread_inf.aclk,Bread_inf.aresetn);

logic          m2s_addr;

axi_stream_interconnect_M2S  #(
    .NUM        (2)
)axi_stream_interconnect_M2S_inst(
/*  input [NSIZE-1:0]     */ .addr      (m2s_addr   ),
/*  axi_stream_inf.slaver */ .s00       (s00        ),// [NUM-1:0],
/*  axi_stream_inf.master */ .m00       (m00        )
);

assign m00.axis_tready  = m00.axis_tdata[RAM_SIZE+DSIZE]? (1'b1 && !rd_ram_en) : (m00_post.ready && !rd_ram_en);        // <<<----- When rd_ram, pause it

assign s00[0].axis_tvalid    = Bread_inf.axis_tvalid;
assign s00[0].axis_tdata     = {1'b0,Bread_inf.axis_tdata,{DSIZE{1'd0}}};
assign s00[0].axis_tlast     = Bread_inf.axis_tlast;
assign Bread_inf.axis_tready = s00[0].axis_tready;

assign s00[1].axis_tvalid     = Bwrite_inf.axis_tvalid;
assign s00[1].axis_tdata      = {1'b1,Bwrite_inf.axis_tdata};
assign s00[1].axis_tlast      = Bwrite_inf.axis_tlast;
assign Bwrite_inf.axis_tready = s00[1].axis_tready;

data_inf_c_planer #(
    .LAT    (OUT_LAT+1  ),
    .DSIZE  (DSIZE    ),
    .HEAD   ("OFF"     )
)data_inf_c_planer_inst(
/*  input [DSIZE-1:0]  */   .pack_data      (DOB        ),
/*  data_inf_c.slaver  */   .slaver         (m00_post   ),
/*  data_inf_c.master  */   .master         (planer_m00 )        //{slaver.data,pack_data}
);

assign m00_post.data  = {m00.axis_tlast,m00.axis_tdata[DSIZE+:RAM_SIZE]};
assign m00_post.valid = m00.axis_tvalid && !m00.axis_tdata[RAM_SIZE+DSIZE] && !rd_ram_en;       // <<<-------- When rd_ram, pause it

assign Bread_ref_inf.axis_tdata = planer_m00.data[RAM_SIZE+DSIZE-1:0];
assign Bread_ref_inf.axis_tlast = planer_m00.data[RAM_SIZE+DSIZE];
assign Bread_ref_inf.axis_tvalid= planer_m00.valid;
assign planer_m00.ready         = Bread_ref_inf.axis_tready;

//--->> EX WR RAM <<-----------------------------------------------
always@(posedge CLKB,posedge RSTB)
    if(RSTB)    ADDRB   <= '0;
    else  begin
        if(rd_ram_en)
                ADDRB   <= rd_ram_addr;
        else if(m00.axis_tvalid && m00.axis_tready)
                ADDRB   <= m00.axis_tdata[DSIZE+:RAM_SIZE];
        else    ADDRB   <= ADDRB;
    end

always@(posedge CLKB,posedge RSTB)
    if(RSTB)    DIB   <= '0;
    else  begin
        if(m00.axis_tvalid && m00.axis_tready && m00.axis_tdata[RAM_SIZE+DSIZE])
                DIB   <= m00.axis_tdata[DSIZE-1:0];
        else    DIB   <= DIB;
    end

assign ENB = 1'b1;

always@(posedge CLKB,posedge RSTB)
    if(RSTB)    WEB   <= '0;
    else  begin
        if(m00.axis_tvalid && m00.axis_tready && m00.axis_tdata[RAM_SIZE+DSIZE])
                WEB   <= 1'b1;
        else    WEB   <= 0;
    end
//---<< EX WR RAM >>-----------------------------------------------
logic       lock_br;
logic       lock_bw;

always@(posedge CLKB,posedge RSTB)
    if(RSTB)    lock_br <= 1'b0;
    else begin
        if(Bread_inf.axis_tvalid && Bread_inf.axis_tready && Bread_inf.axis_tlast)
                lock_br <= 1'b0;
        else if(Bread_inf.axis_tvalid && Bread_inf.axis_tready)
                lock_br <= 1'b1;
        else    lock_br <= lock_br;
    end

always@(posedge CLKB,posedge RSTB)
    if(RSTB)    lock_bw <= 1'b0;
    else begin
        if(Bwrite_inf.axis_tvalid && Bwrite_inf.axis_tready && Bwrite_inf.axis_tlast)
                lock_bw <= 1'b0;
        else if(Bwrite_inf.axis_tvalid && Bwrite_inf.axis_tready)
                lock_bw <= 1'b1;
        else    lock_bw <= lock_bw;
    end

always@(posedge CLKB,posedge RSTB)
    if(RSTB)    m2s_addr    <= 1'b0;
    else begin
        if(lock_br && !m2s_addr)
                m2s_addr    <= 1'b0;
        else if(lock_bw && m2s_addr)
                m2s_addr    <= 1'b1;
        else if(Bwrite_inf.axis_tvalid && !Bread_inf.axis_tvalid)
                m2s_addr    <= 1'b1;
        else if(!Bwrite_inf.axis_tvalid && Bread_inf.axis_tvalid)
                m2s_addr    <= 1'b0;
        else if(!Bwrite_inf.axis_tvalid && !Bread_inf.axis_tvalid)
                m2s_addr   <= !m2s_addr;
        else    m2s_addr    <= m2s_addr;
    end

//---<< B SIZE >>------------------------------------
endmodule
