/**********************************************
______________                ______________
______________ \  /\  /|\  /| ______________
______________  \/  \/ | \/ | ______________

descript:
mail: wmy367@gmail.com
Version: VERA.0.0
creaded: 2018/7/10 上午9:15:13
madified:
***********************************************/
`timescale 1ns/1ps
module core_bram_sim #(
    parameter INIT_FILE = "NONE",
    parameter RAM_SIZE   = 10,
    parameter DSIZE      = 18
)(
    input                       CLKA,
    input                       RSTA,
    input [15:0]                ADDRA,
    // input [DSIZE-1:0]           DIA,
    input [32-1:0]              DIA,
    input                       ENA,
    input                       WEA,
    // output logic[DSIZE-1:0]     DOA,
    output logic[32-1:0]        DOA,

    input                       CLKB,
    input                       RSTB,
    input [15:0]                ADDRB,
    // input [DSIZE-1:0]           DIB,
    input [32-1:0]              DIB,
    input                       ENB,
    input                       WEB,
    // output logic[DSIZE-1:0]     DOB
    output logic[32-1:0]        DOB
);

logic[DSIZE-1:0]        mem     [2**RAM_SIZE-1:0];

initial begin
    $readmemh(INIT_FILE,mem);
end
//port A
logic [DSIZE-1:0]       pre_doa;

always@(posedge CLKA)begin:AWR_PORT
    if(ENA && WEA)begin
        mem[ADDRA]     <= DIA[DSIZE-1:0];
    end
end

always@(posedge CLKA)begin:ARD_PORT
    if(ENA && !WEA)begin
        pre_doa <= mem[ADDRA];
    end
end

//port B
logic [DSIZE-1:0]       pre_dob;

always@(posedge CLKB)begin:BWR_PORT
    if(ENB && WEB)begin
        mem[ADDRB]     <= DIB[DSIZE-1:0];
    end
end

always@(posedge CLKB)begin:BRD_PORT
    if(ENB && !WEB)begin
        pre_dob <= mem[ADDRB];
    end
end

// --- latency ---------

latency #(
    .LAT        (2      ),
    .DSIZE      (DSIZE  )
)latency_ainst(
/*  input                */  .clk           (CLKA           ),
/*  input                */  .rst_n         (~RSTA          ),
/*  input [DSIZE-1:0]    */  .d             (pre_doa        ),
/*  output[DSIZE-1:0]    */  .q             (DOA[DSIZE-1:0] )
);

latency #(
    .LAT        (2      ),
    .DSIZE      (DSIZE  )
)latency_binst(
/*  input                */  .clk           (CLKB           ),
/*  input                */  .rst_n         (~RSTB          ),
/*  input [DSIZE-1:0]    */  .d             (pre_dob        ),
/*  output[DSIZE-1:0]    */  .q             (DOB[DSIZE-1:0] )
);

endmodule
