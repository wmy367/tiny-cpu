/**********************************************
______________                ______________
______________ \  /\  /|\  /| ______________
______________  \/  \/ | \/ | ______________

descript:
mail: wmy367@gmail.com
Version: VERA.1.0 2018-6-21 14:32:10
    compact cmd
Version: VERA.2.0 2018-6-21 15:37:31
    data = 12
    cmd  = 6
Version: VERA.2.1 2018/8/10 上午10:02:02
    add update_live_enable
creaded: 2018-5-9 16:54:27
madified:
***********************************************/
`timescale 1ns/1ps
module Tiny_CPU_core_A2 #(
    // parameter INIT_FILE = "NONE",
    parameter WR_REG_NUM    = 8+10,
    parameter RD_REG_NUM    = 8+8,
    parameter TRI_REG_NUM   = 8,
    parameter RAM_SIZE      = 10,
    parameter DSIZE         = 18
)(
    // input                   queue_start,
    output logic [DSIZE-1:0]            wr_regs  [WR_REG_NUM-1:0],
    output logic [WR_REG_NUM-1:0]       wr_regs_update,
    input  logic [DSIZE-1:0]            rd_regs  [RD_REG_NUM-1:0],
    input  logic [RD_REG_NUM-1:0]       rd_regs_update,
    data_inf_c.master                   trigger_inf [TRI_REG_NUM-1:0],
    output logic                        busy,
    axi_stream_inf.slaver               update_ram_inf,          // DSIZE = 18 + [10-12]
    axi_stream_inf.slaver               Bread_inf,               // DSIZE = RAM_SIZE
    axi_stream_inf.master               Bread_ref_inf,           // DSIZE = RAM_SIZE + DSIZE
    tpu_ram_inf.master                  ram_inf,
    input  logic                        update_live_enable,
    output logic                        update_live_open
);

import SystemPkg::*;
import TPU_CMD_PKG::*;

logic       clock,rst_n;

assign  clock   = update_ram_inf.aclk;
assign  rst_n   = update_ram_inf.aresetn;

logic             queue_start;      //when update addr[0] ,set 1

genvar KK;
//---->> STANDARD CMD EXEC <<--------------------------------
logic                           queue_reset;
data_inf_c #(DSIZE + RAM_SIZE)  queue_inf(clock,rst_n);
data_inf_c #(1)                 proc_done_inf(clock,rst_n);
data_inf_c #(1)                 trigger_start(clock,rst_n);

logic                       jump;
logic [RAM_SIZE-1:0]        jump_addr;
logic                       bak_stack;
logic                       update;
logic [RAM_SIZE-1:0]        update_addr;
logic [DSIZE-1:0]           update_data;
logic                       rd_ram_en;
logic [RAM_SIZE-1:0]        rd_ram_addr;
logic [DSIZE-1:0]           rd_ram_data;


queue_cmd_process_A2 #(
    .WR_REG_NUM         (WR_REG_NUM     ),
    .RD_REG_NUM         (RD_REG_NUM     ),
    .TRI_REG_NUM        (TRI_REG_NUM    ),
    .RAM_SIZE           (RAM_SIZE       ),
    .DSIZE              (DSIZE          )
)queue_cmd_process_inst(
    //-
// /*  input                         */  .start            (queue_start    ),
/*  data_inf_c.slaver             */  .trigger_start    (trigger_start  ),
/*  output logic [15:0]           */  .wr_regs          (wr_regs        ),//[WR_REG_NUM-1:0],
/*  output logic [WR_REG_NUM-1:0] */  .wr_regs_update   (wr_regs_update ),
/*  input  logic [15:0]           */  .rd_regs          (rd_regs        ),//[RD_REG_NUM-1:0],
/*  input  logic [RD_REG_NUM-1:0] */  .rd_regs_update   (rd_regs_update ),
// trigger inf
/*  data_inf_c.master             */  .trigger_inf      (trigger_inf    ),//[TRI_REG_NUM-1:0],
// wait hold
    // jump cmd
/*  output logic                  */  .jump             (jump           ),
/*  output logic [9:0]            */  .jump_addr        (jump_addr      ),
    // wr back to ram
/*  output logic                  */  .update           (update         ),
/*  output logic [9:0]            */  .update_addr      (update_addr    ),
/*  output logic [15:0]           */  .update_data      (update_data    ),
// rd from ram
/*  output logic                  */  .rd_ram_en        (rd_ram_en      ),
/*  output logic [9:0]            */  .rd_ram_addr      (rd_ram_addr    ),
/*  input [15:0]                  */  .rd_ram_data      (rd_ram_data    ),        //delay 2 clock
    // queue
/*   data_inf_c.master            */  .proc_done_inf    (proc_done_inf  ),
/*  output                        */  .queue_reset      (queue_reset    ),
/*  data_inf_c.slaver             */  .queue_inf        (queue_inf      ),
/*  input  logic                  */  .update_live_enable   (update_live_enable ),
/*  output logic                  */  .update_live_open     (update_live_open   )
);


assign proc_done_inf.ready  = 1'b1;
//----<< STANDARD CMD EXEC >>--------------------------------
//---->> RAM BLOCK <<----------------------------------------
localparam BRAM_LAT = 3;
localparam NUM      = 2;

// axi_stream_inf #(RAM_SIZE)          Bread_inf                      (clock,rst_n,1'b1);
axi_stream_inf #(RAM_SIZE)          sub_Bread_inf [NUM-1:0]        (clock,rst_n,1'b1);
// axi_stream_inf #(RAM_SIZE+DSIZE)    Bread_ref_inf                  (clock,rst_n,1'b1);
axi_stream_inf #(RAM_SIZE+DSIZE)    sub_Bread_ref_inf [NUM-1:0]    (clock,rst_n,1'b1);

axi_stream_inf #(RAM_SIZE+DSIZE)    Bwrite_inf                     (clock,rst_n,1'b1);
// axi_stream_inf #(RAM_SIZE+DSIZE)    sub_Bwrite_inf [NUM-1:0]       (clock,rst_n,1'b1);



// axi_stream_interconnect_M2S #(
//     .NUM    (NUM)
// )axi_stream_interconnect_M2S_noaddr_inst(
// /*    input [NSIZE-1:0]   */ .addr      (Br_addr        ),
// /*  axi_stream_inf.slaver */ .s00       (sub_Bread_inf  ),//[NUM-1:0],
// /*  axi_stream_inf.master */ .m00       (Bread_inf      )
// );
//
// axi_stream_interconnect_M2S #(
//     .NUM    (NUM)
// )write_noaddr_inst(
// /*    input [NSIZE-1:0]   */ .addr      (Bw_addr        ),
// /*  axi_stream_inf.slaver */ .s00       (sub_Bwrite_inf  ),//[NUM-1:0],
// /*  axi_stream_inf.master */ .m00       (Bwrite_inf      )
// );

// axi_stream_interconnect_S2M #(
//     .NUM        (NUM      )
// )axi_stream_interconnect_S2M_ref_inf(
// /*  input [NSIZE-1:0]     */ .addr      (Br_addr            ),
// /*  axi_stream_inf.slaver */ .s00       (Bread_ref_inf      ),
// /*  axi_stream_inf.master */ .m00       (sub_Bread_ref_inf  )//[NUM-1:0]
// );

data_inf_c #(1) queue_start_inf (clock,rst_n);

master_bram_A2 #(
    // .INIT_FILE      (INIT_FILE  ),
    .OUT_LAT        (BRAM_LAT   ),
    .RAM_SIZE       (RAM_SIZE   ),
    .DSIZE          (DSIZE      )
)master_bram_inst(
  //-- A SIZE
/*  input                */  .clock             (clock          ),
/*  input                */  .rst_n             (rst_n          ),
/*  data_inf_c.slaver    */  .boot_start_inf    (queue_start_inf),
/*  input                */  .jump              (jump           ),
/*  input [9:0]          */  .jump_addr         (jump_addr      ),
/*  input                */  .update            (update         ),
/*  input [9:0]          */  .update_addr       (update_addr    ),
/*  input [15:0]         */  .update_data       (update_data    ),
/*  input                */  .rd_ram_en         (rd_ram_en      ),
/*  input [9:0]          */  .rd_ram_addr       (rd_ram_addr    ),
/*  output[15:0]         */  .rd_ram_data       (rd_ram_data    ),
/*  data_inf_c.mirror    */  .proc_done_inf     (proc_done_inf  ),
/*  input                */  .queue_reset       (queue_reset    ),
/*  data_inf_c.master    */  .queue_inf         (queue_inf      ),
/*  axi_stream_inf.slaver*/  .Bread_inf         (Bread_inf      ),  // DSIZE = 10
/*  axi_stream_inf.master*/  .Bread_ref_inf     (Bread_ref_inf  ),  // DSIZE = 10+16
/*  axi_stream_inf.slaver*/  .Bwrite_inf        (Bwrite_inf     ),   // DSIZE = 10+16
/*  tpu_ram_inf.master   */  .ram_inf           (ram_inf        )
);

// assign      Bread_inf.axis_tvalid         = 1'b0;
// assign      Bread_ref_inf.axis_tready     = 1'b1;

axis_direct axis_direct_inst(
/*  axi_stream_inf.slaver  */   .slaver     (update_ram_inf ),
/*  axi_stream_inf.master  */   .master     (Bwrite_inf )
);

//----<< RAM BLOCK >>----------------------------------------
//---->> TRIGGER START <<------------------------------------
trigger_data_inf_c #(
    .DSIZE      (1)
)trigger_data_inf_c_start_inf(
/*  input             */  .trigger      (update_ram_inf.axis_tvalid && update_ram_inf.axis_tready && update_ram_inf.axis_tdata[DSIZE+:RAM_SIZE] == '0),
/*  input [DSIZE-1:0] */  .data         (1'b0),
/*  data_inf_c.master */  .trigger_inf  (trigger_start  )
);

trigger_data_inf_c #(
    .DSIZE      (1)
)trigger_data_inf_c_queue_start_inf(
/*  input             */  .trigger      (update_ram_inf.axis_tvalid && update_ram_inf.axis_tready && update_ram_inf.axis_tdata[DSIZE+:RAM_SIZE] == '0),
/*  input [DSIZE-1:0] */  .data         (1'b0),
/*  data_inf_c.master */  .trigger_inf  (queue_start_inf  )
);
assign busy         = ~trigger_start.ready;
//----<< TRIGGER START >>------------------------------------
endmodule
