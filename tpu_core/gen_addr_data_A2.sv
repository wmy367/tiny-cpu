/**********************************************
______________                ______________
______________ \  /\  /|\  /| ______________
______________  \/  \/ | \/ | ______________

descript:
mail: wmy367@gmail.com
Version: VERB.0.0 2018-5-10 16:44:36
Version: VERA.2.0 2018-6-21 15:56:39
    data == 18
creaded: 2018-3-26 11:06:04
madified:
***********************************************/
`timescale 1ns/1ps
module gen_addr_data_A2 #(
    parameter       CORE_ID = 0,
    parameter       ID_BASE_ADDR = 1024-32,
    parameter       RAM_SIZE    = 10
)(
    input                   clock,
    input                   rst_n,
    input                   wr_core_id,
    input                   queue_reset,
    data_inf_c.slaver       boot_start_inf,
    input                   proc_done,
    input                   jump,
    input [RAM_SIZE-1:0]    jump_addr,
    input                   update,
    input [RAM_SIZE-1:0]    update_addr,
    input [17:0]            update_data,
    // output logic            valida,
    // output logic[RAM_SIZE-1:0]       addra,
    data_inf_c.master       addra_inf,
    // input [RAM_SIZE-1:0]             bak_addra,
    input                   hold_addr,
    output logic[17:0]      dataa,
    output logic            wena,
    data_inf_c.mirror       queue_inf
);
logic[RAM_SIZE-1:0]         addra;
logic                       valida;

assign  addra_inf.valid     = valida;
assign  addra_inf.data      = addra;

int Acnt;
logic [3:0]     a_asgn;

assign a_asgn = {wr_core_id,boot_start_inf.valid & boot_start_inf.ready,jump,update};

always@(posedge clock,negedge rst_n)// one clockm, only one cmd can be assigned
    if(~rst_n)
        Acnt <= 0;
    else begin
        Acnt = 0;
        foreach(a_asgn[i])begin
            if(a_asgn[i])
                Acnt = Acnt + 1;
        end

        assert(Acnt <= 1)
        else begin
            $error("\none clockm, only one cmd can be assigned\n");
            $stop;
        end
    end

typedef enum {
    NOP,
    IDLE,
    WR_ID,
    JUMP,
    UPDATE,
    WAIT_N_UPDATE,
    RELOAD_NOR,
    START,
    NORMAL
}   STATUS;


STATUS  nstate,cstate;

always@(posedge clock,negedge rst_n)
    if(~rst_n)  cstate  <= NOP;
    else        cstate  <= nstate;

always_comb begin
    case(cstate)
    NOP:
        if(boot_start_inf.valid && boot_start_inf.ready)
                nstate  = IDLE;
        else    nstate  = NOP;
    IDLE:
        if(~queue_reset)
                nstate  = START;
        else    nstate  = IDLE;
    START:      nstate  = NORMAL;
    NORMAL:
        if(jump)
                nstate  = JUMP;
        else if(proc_done)
                nstate  = NOP;
        else if(update)
                nstate  = UPDATE;
        // else if(update_addr==ID_BASE_ADDR)
        //         nstate  = NOP;
        else    nstate  = NORMAL;
    JUMP:
        if(queue_inf.ready)
        // if(queue_inf.ready && queue_inf.valid)
                nstate  = NORMAL;
        else    nstate  = JUMP;
    UPDATE:
        if(!queue_inf.ready)
                nstate  = UPDATE;
        else if(~update)
                nstate  = RELOAD_NOR;
        else    nstate  = WAIT_N_UPDATE;
    WAIT_N_UPDATE:
        if(!queue_inf.ready)
                nstate  = WAIT_N_UPDATE;
        else if(~update)
                nstate  = RELOAD_NOR;
        else    nstate  = WAIT_N_UPDATE;
    RELOAD_NOR:
        if(jump)
                nstate  = JUMP;
        else    nstate  = NORMAL;
    default:    nstate  = NOP;
    endcase
end
//---->> STATUS CTRL <<---------------
logic[$bits(addra)-1:0] addra_record;

always@(posedge clock,negedge rst_n)
    if(~rst_n)  addra   <= '0;
    else begin
        case(nstate)
        NOP,IDLE,START:
                addra   <= '0;
        NORMAL:begin
           // addra   <= addra + (queue_inf.ready);
            // if(hold_addr)
            //         addra   <= addra;
            // else if(queue_inf.ready)
            //         addra   <= addra + 1'b1;
            // else begin
            //         addra   <= addra;
            // end
            addra   <= addra + (addra_inf.ready && addra_inf.valid);
        end
        JUMP:   addra   <= jump_addr;
        WAIT_N_UPDATE,
        UPDATE: addra   <= update_addr;
        RELOAD_NOR:
                addra   <= addra_record;
        default:;
        endcase
    end

always@(posedge clock,negedge rst_n)
    if(~rst_n)  addra_record   <= '0;
    else begin
        case(nstate)
        UPDATE: addra_record   <= addra + queue_inf.ready;
        default:;
        endcase
    end

always@(posedge clock,negedge rst_n)
    if(~rst_n)  dataa   <= '0;
    else begin
        case(nstate)
        NOP,IDLE,START:
                dataa   <= '0;
        NORMAL: dataa   <= '0;
        JUMP:   dataa   <= '0;
        WAIT_N_UPDATE,
        UPDATE: dataa   <= update_data;
        RELOAD_NOR:
                dataa   <= '0;
        default:;
        endcase
    end

always@(posedge clock,negedge rst_n)
    if(~rst_n)  wena   <= '0;
    else begin
        case(nstate)
        NOP,IDLE,START:
                wena   <= '0;
        NORMAL: wena   <= '0;
        JUMP:   wena   <= '0;
        WAIT_N_UPDATE,
        UPDATE: wena   <= 1'b1;
        RELOAD_NOR:
                wena   <= '0;
        default:;
        endcase
    end

always@(posedge clock,negedge rst_n)
    if(~rst_n)  valida   <= '0;
    else begin
        case(nstate)
        NOP,IDLE:
                valida   <= '0;
        START:  valida   <= '1;
        NORMAL: valida   <= '1;
        JUMP:   valida   <= '1;
        WAIT_N_UPDATE,
        UPDATE: valida   <= 1'b1;
        RELOAD_NOR:
                valida   <= '1;
        default:;
        endcase
    end
//----<< STATUS CTRL >>---------------
//---->> BOOT START <<----------------
always@(posedge clock,negedge rst_n)
    if(~rst_n)  boot_start_inf.ready   <= '0;
    else begin
        case(nstate)
        NOP:    boot_start_inf.ready    <= 1'b1;
        default:boot_start_inf.ready    <= 1'b0;
        endcase
    end
//----<< BOOT START >>----------------
endmodule
