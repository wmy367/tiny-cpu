/**********************************************
______________                ______________
______________ \  /\  /|\  /| ______________
______________  \/  \/ | \/ | ______________
descript:
mail: wmy367@gmail.com
Version: VERA.0.0
created: 2018-06-27 16:00:37 +0800
madified:
***********************************************/
`timescale 1ns/1ps

module unuse_base_bread(
    axi_stream_inf.slaver     Bread_inf,
    axi_stream_inf.master     Bread_ref_inf
);

assign  Bread_inf.axis_tvalid       = 1'b0;
assign  Bread_ref_inf.axis_tready   = 1'b1;

endmodule
