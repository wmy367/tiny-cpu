
interface tpu_ram_inf #(
    parameter   DSIZE = 18,
    parameter   RSIZE = 10,
    parameter   MSIZE = 1
)();

logic                       clka;
logic                       rsta;
logic[15:0]                 addra;
logic[DSIZE-1:0]            dia;
logic                       ena;
logic[MSIZE-1:0]            wea;
logic[DSIZE-1:0]            doa;

logic                       clkb;
logic                       rstb;
logic[15:0]                 addrb;
logic[DSIZE-1:0]            dib;
logic                       enb;
logic[MSIZE-1:0]            web;
logic[DSIZE-1:0]            dob;

modport master (
    output           clka,
    output           rsta,
    output           addra,
    output           dia,
    output           ena,
    output           wea,
    input            doa,

    output           clkb,
    output           rstb,
    output           addrb,
    output           dib,
    output           enb,
    output           web,
    input            dob
);

modport slaver (
    input            clka,
    input            rsta,
    input            addra,
    input            dia,
    input            ena,
    input            wea,
    output           doa,

    input            clkb,
    input            rstb,
    input            addrb,
    input            dib,
    input            enb,
    input            web,
    output           dob
);

endinterface
