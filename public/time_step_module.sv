/**********************************************
______________              ______________
______________ XPECTVISION  ______________
______________              ______________

descript:
author : Young
Version: VERA.0.0
creaded: 2017/1/23 下午3:12:03
madified:
***********************************************/
`timescale 1ns/1ps
(* data_inf = "true" *)
module time_step_module #(
    parameter       SYS_CLK_FREQ = 100,      // Mhz
    parameter real  Xms          = 1.0,      // 1ms step
    parameter real  Sim_Xms      = 0.00032
)(
    input           clock,
    output logic    step_pulse
);

import SystemPkg::*;

localparam  real CLK_P   = 1000.0/SYS_CLK_FREQ;

parameter int  MAX     = (Xms/CLK_P) * 1000 * 1000;    //ns
parameter int  SIM_MAX = (Sim_Xms/CLK_P) * 1000 * 1000;    //ns

logic [31:0]    cnt = 32'd0;

always@(posedge clock)begin
    if(SystemPkg::SIM=="ON"||SystemPkg::SIM=="TRUE")begin
        if(cnt < SIM_MAX)
                cnt <= cnt + 1'b1;
        else    cnt <= 31'd0;
    end else begin
        if(cnt < int'(MAX))
                cnt <= cnt + 1'b1;
        else    cnt <= 31'd0;
    end
end

always@(posedge clock)
    step_pulse  <= cnt == 32'd0;


endmodule
