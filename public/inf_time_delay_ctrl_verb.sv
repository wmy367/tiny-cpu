/**********************************************
______________                ______________
______________ \  /\  /|\  /| ______________
______________  \/  \/ | \/ | ______________
descript:
mail: wmy367@gmail.com
Version: VERB.0.0 :2018-6-11 10:55:09
    direct ctrl time
created: 2018-4-24 11:36:29
madified:
***********************************************/
`timescale 1ns/1ps
module inf_time_delay_ctrl_verb #(
    parameter       SYS_CLK_FREQ    = 100,      // Mhz
    parameter real  STEP_Xms        = 1.0,      // 1ms step
    parameter real  STEP_Sim_Xms    = 0.00032
)(
    output logic        done,
    data_inf_c.slaver   trigger_inf         //
);


logic   step_pulse;

time_step_module #(
    .SYS_CLK_FREQ   (SYS_CLK_FREQ),      // Mhz
    .Xms            (STEP_Xms    ),      // 1ms step
    .Sim_Xms        (STEP_Sim_Xms)
)time_step_module(
/*  input         */  .clock        (trigger_inf.clock  ),
/*  output logic  */  .step_pulse   (step_pulse         )
);

inf_time_delay_ctrl inf_time_delay_ctrl_inst(
/*  input              */ .step_pulse       (step_pulse     ),
/*  data_inf_c.slaver  */ .trigger_inf      (trigger_inf    )     //
);

assign  done = trigger_inf.valid && trigger_inf.ready;

endmodule
