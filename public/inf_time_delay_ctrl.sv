/**********************************************
______________                ______________
______________ \  /\  /|\  /| ______________
______________  \/  \/ | \/ | ______________
descript:
mail: wmy367@gmail.com
Version: VERA.0.0
created: 2018-4-24 11:36:29
madified:
***********************************************/
`timescale 1ns/1ps
module inf_time_delay_ctrl (
    input               step_pulse,
    data_inf_c.slaver   trigger_inf         //
);

logic [trigger_inf.DSIZE-1:0]        cnt;

logic   clock,rst_n;

assign  clock   = trigger_inf.clock;
assign  rst_n   = trigger_inf.rst_n;

always@(posedge clock,negedge rst_n)
    if(~rst_n)   cnt     <= '0;
    else
        if(~trigger_inf.valid)
                cnt     <= '0;
        else if(trigger_inf.valid && trigger_inf.ready)
                cnt     <= '0;
        else if(trigger_inf.valid && step_pulse)
                cnt     <= cnt + 1'b1;
        else    cnt     <= cnt;


always@(posedge clock,negedge rst_n)
    if(~rst_n)  trigger_inf.ready   <= 1'b0;
    else begin
        if(trigger_inf.ready && trigger_inf.valid)
                trigger_inf.ready   <= 1'b0;
        else if(trigger_inf.valid)
                trigger_inf.ready <= cnt >= trigger_inf.data;
        else    trigger_inf.ready   <= 1'b0;
    end

endmodule
