/**********************************************
______________                ______________
______________ \  /\  /|\  /| ______________
______________  \/  \/ | \/ | ______________
descript:
mail: wmy367@gmail.com
Version: VERA.0.0
created: 2018/12/15 下午4:51:19
madified:
***********************************************/
`timescale 1ns/1ps
module tb_demo_tpu();


logic       clock;
logic       rst_n;

initial begin
    clock   = 0;
    rst_n   = 0;
    #(1us);
    rst_n   = 1;
end

always #(10ns) clock = ~clock;

axi_stream_inf #(10+16) boot_inf (clock,rst_n,1'b1);

demo_tpu #(
    .INIT_FILE      ("C:/work/TPU/tiny-cpu/demo_tpu/demo_tpu.coe" ),
    .RAM_SIZE       (10             ),
    .DSIZE          (16             )
)demo_tpu_inst(
/*  output logic              */      .sda_out              (),
/*  output logic              */      .spi_mosi             (),
/*  input                     */      .sda_in               (1'b1),
/*  input                     */      .spi_miso             (1'b0),
/*  output logic[1-1:0]       */      .scl                  (),
/*  output logic[1-1:0]       */      .tri_ctrl             (),
/*  output logic[1-1:0]       */      .spi_sclk             (),
/*  output logic[1-1:0]       */      .spi_csn              (),
/*  input                     */      .update_live_enable   (1'b0),
/*  output logic              */      .update_live_open     (),
/*  output logic              */      .busy                 (),
/*  axi_stream_inf.slaver     */      .update_ram_inf       (boot_inf   )          //DSIZE = 26
);

nop_just_init_file #(
    .RAM_SIZE       (10)
)nop_just_init_file_inst(
/*  axi_stream_inf.master   */  .inf        (boot_inf   )
);


endmodule
