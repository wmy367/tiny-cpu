/**********************************************
______________              ______________
______________ XPECTVISION  ______________
______________              ______________

descript:
author : Young
Version: VERA.0.0
creaded: 2018-6-3 16:33:01
madified:
***********************************************/
`timescale 1ns/1ps
module nop_just_init_file #(
    parameter RAM_SIZE = 10
)(
    axi_stream_inf.master   inf
);


logic       clock,rst_n;

assign  clock   = inf.aclk;
assign  rst_n   = inf.aresetn;

// import AxiBfmPkg::*;

import TPU_CMD_PKG::*;

typedef struct {
    logic[RAM_SIZE-1:0]      addr;
    logic[5:0]      cmd;
    logic[inf.DSIZE-1-6-RAM_SIZE:0]     data;
} ad_c;

typedef struct {
    logic[RAM_SIZE-1:0]         addr;
    logic[inf.DSIZE-1:0]        data;
} dd_c;


// AxiStreamMasterBfm_c #(
//     .DSIZE  (inf.DSIZE)
// )boot_bfm = new(inf);


logic  [inf.DSIZE-1:0] data_s [$];

initial begin
    wait(rst_n);
    // #(10ms);
    boot_jump();
    #(1us);
end

task automatic boot_jump();
ad_c ad0;
ad_c ad1;

    ad0.addr = 0;
    ad0.cmd = CMD_JUMP;
    ad0.data = 1;

    data_s = {>>{ad0}};
    // boot_bfm.gen_axi_stream(0,100,data_s);
    inf.axis_tdata  = data_s[0];
    inf.axis_tvalid = 1;
    inf.axis_tlast  = 1;
    wait(inf.axis_tready);
    @(posedge clock);
    inf.axis_tvalid = 0;
    inf.axis_tlast  = 0;
endtask:boot_jump

// logic   scl,sda_out;
//
// assign scl      = $root.tb_Tiny_CPU_0510.Tiny_CPU_inst.wr_regs[0][1];
// assign sda_out  = $root.tb_Tiny_CPU_0510.Tiny_CPU_inst.wr_regs[0][0];

endmodule
