## 文件说明 ##

### demo_tpu
- _自动生成的tpu_
- demo_tpu_cfg_pkg.sv :
    + 定制tpu所配置的参数
- demo_tpu_coe.coe ：
    + Xilinx RAM IP 初始化文件
- demo_tpu_ex_units :
    + 计算单元模块
- demo_tpu.coe.mif
    + RAM 二进制 内容
- demo_tpu.coe.txt
    + RAM 程序对应的汇编内容
- demo_tpu.sv
    + 顶层文件

### fifo
- common_fifo.sv
    + 同时钟 fifo

### interface
- _定义接口_
- axi_stream_inf.sv
    + axi stream 接口
- data_interface_pkg.sv
    + data_interface 用到的基本函数
- data_interface_pkg.sv
    + data 基本接口

### interface_module
- _使用到接口的模块_
- axi_stream_interconnect_M2S
    + axis 多口到单口的 内部连接
- axis_direct
    + axis 直连
- data_inf_A2B，data_inf_B2A
    + data 接口类型转换
- data_inf_c_planer,data_inf_c_planer_A1
    + 用于从RAM 推出数据 并拼接
- data_pipe_interconnect_M2S_verb
    + data接口 多口到单口的 内部连接
- trigger_data_inf_c
    + 触发 data接口 master

### public
- _公用模块_
- inf_time_delay_ctrl,inf_time_delay_ctrl_verb
    + 延时控制，使用data接口
- latency
    + 延时打拍

### SystemPkg
- _定义系统级参数_

### tpu_core
- _tpu内核模块_
- core_bram_default，core_bram_sim
    + RAM 仅用于仿真
- gen_addr_data_A2
    + 生成 RAM 地址，用于命令生成
- master_bram_A2
    + tpu RAM 包装
- queue_cmd_prcess_A2
    + tpu 命令处理模块
- Tiny_CPU_core_A2
    + tpu 核心模块 top
- TPU_CMD_PKG
    + tpu 指令
- tpu_ram_interface
    + tpu ram 接口定义
- unuse_base_bread
    + 拓展 axi4 axilite，非使用情况下使用
